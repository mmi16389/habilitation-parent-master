import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import { DialogService } from 'ng2-bootstrap-modal';
import {CustomizeDialogueModalComponent} from '../modules/workflow-shared/custumize-dialogue-modal/customize-dialogue-modal.component';
import { UrlAppContext } from '../../assets/url-context/url-app-context';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private dialogService: DialogService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {

      // Gestion des erreurs 403
      if (err.status === 403) {

        const tokenExpiry = (JSON.parse(localStorage.getItem('user')).exp);
        const tokenExpired = (tokenExpiry * 1000) < new Date().valueOf();

        // Si le token JWT a expiré, renvoi vers la page d'accueil
        if (tokenExpired) {
          localStorage.clear();
          setTimeout(() => {
            location.reload(true);
          });
        } else { // Sinon, affichage d'une popup
          this.dialogService.addDialog(CustomizeDialogueModalComponent, {
            title: 'Erreur',
            action: {value: 'Vous n\'êtes pas autorisé à effectuer cette action'}
          });
        }

      }

      // Gestion des erreurs back
      if (err.status === 500) {
        
      }

      const error = err.error.message || err.statusText;
      return throwError(error);
    }));
  }
}
