export class Site {

  public id: number;
  public code: string;
  public libelle: string;
  public adg2Mail: string;
  public codeGh: string;
  public adg2Telephone: string;
  public responsableSecuriteTelephone: string;
  public imprimante: string;
  

  constructor() {
  }
}
