import { Site } from "./site";

export class Utilisateur {

  public codeAPH: string;
  public nom: string;
  public prenom: string;
  public token:string;
  public telephone: string;
  public site: Site;

  constructor() {
  }
}
