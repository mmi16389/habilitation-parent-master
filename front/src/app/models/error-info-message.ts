
export class ErrorInfoMessage{
    public visible: boolean;
    public type: string;
    public title: string;
    public message: string;
    public code: number;

    constructor() {
    }
}
