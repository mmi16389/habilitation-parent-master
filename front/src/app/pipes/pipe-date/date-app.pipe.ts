import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'dateApp'
})

export class DateAppPipe implements PipeTransform {
    transform(value: any, args?: any): any {
        let formatDate = '';
        if (value !== null && value !== '') {
            const date = new Date(value);
            formatDate = date.toLocaleDateString();
        }
        return formatDate;
    }
}
