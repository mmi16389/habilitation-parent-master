import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TranslateLabelService extends AbstractService {
  private label: string;
  private URL_TRANSLATE_TO_FR = '/assets/i18n/fr.json';
  private URL_TRANSLATE_TO_EN = '/assets/i18n/en.json';

  constructor(private http: HttpClient) {
    super();
    this.label = '';
  }
  /**
   * -----------------------------------------------------------------------------------------------------------------------------------
   * @param label
   * -----------------------------------------------------------------------------------------------------------------------------------
   */
  public getLableforI18n(label: string, lang: string): Observable<any> {
    let url = '';
    localStorage.setItem('lang', lang);
    localStorage.setItem('currentLabel', label);
    switch (lang) {
      case 'fr': url = this.URL_TRANSLATE_TO_FR; break;
      case 'en': url = this.URL_TRANSLATE_TO_EN; break;
      default: url = this.URL_TRANSLATE_TO_FR;
    }
    return this.http.get(url).pipe(this.filterData);//.catch(this.handleObservableError);
  }
  /**
   * -------------------------------------------------------------------------------------------------------------------------------------
   * @param resp
   * ------------------------------------------------------------------------------------------------------------------------------------
   */
  private filterData(response: any) {
    const labelTemp = localStorage.getItem('currentLabel');
    const resp = response[labelTemp];
    return resp;
  }
}
