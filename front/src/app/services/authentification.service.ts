import {Injectable} from '@angular/core';
import {Autification} from '../models/autification';
import {AbstractService} from "./abstract.service";
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService extends AbstractService {
  private appContext = new BehaviorSubject({token: 'azertyuiop.klom'});

  constructor(private http: HttpClient) {
    super();
  }

  /**
   *
   * @param username
   * @param password
   */
  loginJWT(username: string, password: string): Observable<any> {
    const headers = this.getHeaderWidthFormURLDecode();
    let body = `username=${username}&password=${password}`;
    // return this.appContext.asObservable();
    return this.http
      .post<any>(this.apiConnexion, body, { headers: headers })
      .pipe(
        catchError(this.handleObservableError)
      );
  }
}
