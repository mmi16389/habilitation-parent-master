import {environment} from '../../environments/environment';
import {throwError} from 'rxjs';
import {HttpHeaders} from '@angular/common/http';

export class AbstractService {

    /**
     *
     */
    protected apiUtilisateurs = environment.api + 'utilisateurs';
    /**
     *
     */
    protected apiCategoriesDomaine = environment.api + 'categories-domaines';
    /**
     *
     */
    protected apiDomaine = environment.api + 'domaines';

    /**
     *
     */
    protected apiConnexion = environment.api + 'token';

    /**
     *
     */
    protected handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    /**
     *
     * @param error
     */
    protected handleObservableError(error: any) {
        console.error('>>>>>>>>>>>>>>>>', error);
        return throwError(error);
    }

    /**
     *
     */
    protected getHeaderJson(): HttpHeaders {
        return new HttpHeaders({'Content-Type': 'application/json'});
    }

    /**
     *
     * @param token
     */
    protected getHeaderParam(token: any): HttpHeaders {
        const headers = new HttpHeaders({
            'Access-Control-Allow-Origin': '*'
        });
        return headers;
    }

    /**
     *
     */
    protected getHeaderWidthFormURLDecode(): HttpHeaders {
        return new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded', withCredentials: 'true'});
    }

}
