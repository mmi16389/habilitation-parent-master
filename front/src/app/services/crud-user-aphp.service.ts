import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractService } from './abstract.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CrudUserAphpService extends AbstractService {
  /**
   *
   * @param httpClient
   */
  constructor(private httpClient: HttpClient) {
    super();
  }

  /**
   *
   */
  getUserLDAP() {
    return this.httpClient.get(`${this.apiUtilisateurs}`);
  }

  /**
   *
   */
  getListUser() {
    return this.httpClient.get(`${this.apiUtilisateurs}`);
  }

  /**
   * 
   * @param id 
   */
  getUserById(id: number) {
    return this.httpClient.get(`${this.apiUtilisateurs + '/' + id}`);
  }

  /**
   * 
   * @param id 
   * @param domain 
   */
  getGroupUserById(id: number, domain: string) {
    return this.httpClient.get(`${this.apiUtilisateurs + '/' + id + '/groupes?domaine='+domain}`);
  }
  /**
   *
   * @param user
   */
  addNewUser(user: any) {
    return this.httpClient.post(user, `${this.apiUtilisateurs}`);
  }

  /**
   *
   * @param id
   */
  deleteUser(id) {
    return this.httpClient.delete(`${this.apiUtilisateurs + '/' + id}`);
  }

  /**
   *
   * @param user
   */
  public enregistrerUSerLADP(user: any): Observable<any> {
    const headers = this.getHeaderJson();
    return this.httpClient
      .post<any>(`${this.apiUtilisateurs}`,
        JSON.stringify(user),
        { headers })
      .pipe(
        catchError(this.handleObservableError)
      );
  }

}
