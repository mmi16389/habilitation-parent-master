import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractService } from 'src/app/services/abstract.service';

@Injectable({
  providedIn: 'root'
})
export class HabilitationsUserService extends AbstractService {
  
  /**
   *
   * @param httpClient
   */
  constructor(private httpClient: HttpClient) {
    super();
  }


  /**
   *
   * @param iduser
   */
  getListGroupeByUSer(iduser: number) {
    return this.httpClient.get(`${this.apiCategoriesDomaine}`);
  }

  /**
  *
  */
  getGroupsAPHP() {
    return this.httpClient.get(`${this.apiCategoriesDomaine}`);
  }

  /**
   *
   */
  getSiteAPHP() {
    return this.httpClient.get(`${this.apiCategoriesDomaine}`);
  }
}
