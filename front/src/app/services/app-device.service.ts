import {Injectable} from '@angular/core';
import {DeviceDetectorService} from 'ngx-device-detector';

@Injectable({
    providedIn: 'root'
  })
export class AppDeviceService {
    public deviceInfo = null;

    /**
     *
     * @param {AppContextService} appContextService
     */
    constructor(private deviceService: DeviceDetectorService) {
        this.deviceInfo = deviceService.getDeviceInfo();
    }


    get isMobile() {
        return this.deviceService.isMobile();
    }

    get isTablet() {
        return this.deviceService.isTablet();
    }

    get isDesktop() {
        return this.deviceService.isDesktop();
    }

    get getOs() {
        return "pds-" + this.deviceService.os;
    }

    get getBrowser() {
        return "pds-" + this.deviceService.browser;
    }

    get getAttributeDevice() {
        let classDevice;
        if (this.isDesktop) {
            classDevice = 'pds-desktop';
        } else if (this.isMobile) {
            classDevice = 'pds-mobile';
        } else if (this.isTablet) {
            classDevice = 'pds-tablet';
        }
        classDevice += " " + this.getOs + " " + this.getBrowser;

        return classDevice;
    }
}
