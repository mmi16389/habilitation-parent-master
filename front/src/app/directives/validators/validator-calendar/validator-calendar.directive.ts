import {Directive, Input, ElementRef, Renderer} from '@angular/core';
import {NG_VALIDATORS, Validator, FormControl, ValidationErrors} from '@angular/forms';
import * as moment from 'moment';

@Directive({
    selector: '[appValidatorCalendar]',
    providers: [{provide: NG_VALIDATORS, useExisting: ValidatorCalendarDirective, multi: true}]
})
export class ValidatorCalendarDirective implements Validator {

    @Input()
    public errorMessageCalendarValidator: string;

    /**
     *
     * @param elParent
     *
     */
    constructor(private elParent: ElementRef, private renderer: Renderer) {
    }

    /**
     *
     * @param c
     *
     */
    validate(c: FormControl): ValidationErrors {

        const isValidEntryCalendar = !this.isEmptyInput(c.value);
        const message = {
            'calendar': {
                'message': this.errorMessageCalendarValidator
            }
        };
        return isValidEntryCalendar ? null : message;
    }

    /**
     *
     * @param value
     *
     */
    isValidDate(value: string) {
        const dateObj = moment(value).format('DD/MM/YYYY');
        return dateObj !== 'Invalid date' ? true : false;
    }

    /**
     *
     * @param value
     *
     */
    isEmptyInput(value: string) {
        return value === '' || value === null ? true : false;
    }
}
