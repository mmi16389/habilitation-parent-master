import {Directive, ElementRef, Input, Renderer} from '@angular/core';
import {FormControl, NG_VALIDATORS, ValidationErrors, Validator, AbstractControl} from '@angular/forms';


@Directive({
    selector: '[appValidatorCompare]',
    providers: [{provide: NG_VALIDATORS, useExisting: ValidatorCompareDirective, multi: true}]
})
export class ValidatorCompareDirective implements Validator {
    /**
     *
     * @type {string}
     */
    @Input()public errorCompareSizeOfValidator = 'Doit être inférieure ou égale ...';
    /**
     * 
     * @type {string}
     */
    @Input() public tailleToCompare = '0';
    /**
     *
     */
    constructor() {
    }

    /**
     *
     * @param {FormControl} c
     * @returns {ValidationErrors}
     */
    validate(c: FormControl): ValidationErrors {

        let valideSize = true;
        if (c.value > Number(this.tailleToCompare) && Number(this.tailleToCompare) !== 0) {
            valideSize = false;
        }

        const message = {
            'compareSize': {
                'message': this.errorCompareSizeOfValidator
            }
        };
        return valideSize ? null : message;
    }
}


