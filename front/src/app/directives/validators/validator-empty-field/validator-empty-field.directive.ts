import {Directive, ElementRef, Renderer, Input} from '@angular/core';
import {NG_VALIDATORS, Validator, FormControl, ValidationErrors} from '@angular/forms';



@Directive({
    selector: '[appValidatorEmptyField]',
    providers: [{provide: NG_VALIDATORS, useExisting: ValidatorEmptyFieldDirective, multi: true}]
})
export class ValidatorEmptyFieldDirective implements Validator {
    @Input()
    public errorMessageEmptyFieldValidator = 'Le champ est obligatoire';

    /**
     *
     * @param elParent
     *
     */
    constructor(private elParent: ElementRef) {
    }

    /**
     *
     * @param c
     * Method of validation pattern
     *
     */
    validate(c: FormControl): ValidationErrors {

        const isValidField = !this.isEmptyInput(c.value);
        const message = {
            'emptyField': {
                'message': this.errorMessageEmptyFieldValidator
            }
        };
        return isValidField ? null : message;
    }

    /**
     *
     * @param value
     *
     */
    isEmptyInput(value: string) {
        let contrValue = value;
        if (value && contrValue.length > 0) {
            value = contrValue.split(' ').join('');
        }
        return value === '' || value === null ? true : false;
    }

}
