import {FormControl, ValidationErrors, NG_VALIDATORS} from '@angular/forms';
import {Directive} from '@angular/core';
import {DateUtils} from '../../../utils/date-utils';
import {Utils} from '../../../utils/utils';

@Directive({
    selector: '[appCustomizeValidatorCalendar]',
    providers: [{provide: NG_VALIDATORS, useExisting: CustomCalendarDirective, multi: true}]
})
export class CustomCalendarDirective {

    /**
     *
     * @param
     *
     */
    static dateBeforOfCurrentDateDay(c: FormControl): ValidationErrors {
        const isValidField = DateUtils.isValidDateBefore(c.value);
        const message = {
            'calendar': {
                'message': Utils.DATE_BEFORE_CURRENT_DATE
            }
        };
        return isValidField ? null : message;
    }

    /**
     *
     * @param c
     */
    static startDateToValidate(c: FormControl): ValidationErrors {
        const isValidField = DateUtils.isValidDateAfter(c.value, c);
        const message = {
            'calendar': {
                'message': Utils.START_DATE_TO_VALIDATE
            }
        };
        return isValidField ? null : message;
    }

    /**
     *
     * @param c
     */
    static endDateGlobalToValidate(c: FormControl): ValidationErrors {
        const isValidField = DateUtils.isValidDateBefore(c.value);
        const message = {
            'calendar': {
                'message': Utils.END_GOBAL_DATE_TO_VALIDATE
            }
        };
        return isValidField ? null : message;
    }

    /**
     *
     * @param c
     */
    static endDateToValidate(c: FormControl): ValidationErrors {
        const isValidField = DateUtils.isValidDateBefore(c.value);
        const message = {
            'calendar': {
                'message': Utils.END_DATE_TO_VALIDATE
            }
        };
        return isValidField ? null : message;
    }

    /**
     *
     * @param c
     */
    static availabilityDate(c: FormControl): ValidationErrors {
        const isValidField = DateUtils.isValidAvailabilityDater(c.value);
        const message = {
            'calendar': {
                'message': Utils.AVAILABILITY_DATE
            }
        };
        return isValidField ? null : message;
    }
}
