/**
 *
 */
import {FormControl, FormGroup} from '@angular/forms';
import * as moment from 'moment';

export class DateUtils {

  /**
   * add days to date
   * @param date  date reference
   * @param days number of days
   */
  public static addDaysToDate(date: Date, days: number): Date {
    return new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate() + days,
      date.getHours(),
      date.getMinutes(),
      date.getSeconds());
  }

  /**
   *
   * @param date
   * @returns {Date}
   */
  public static dateWithoutHour(date: any): Date {
    if (date) {
      const d = new Date(new Date(date).setHours(0, 0, 0, 0));
      return d;
    }
  }

  /**
   *
   * @param time
   * @returns {Date}
   */
  public static timeToDate(time: any): Date {
    return new Date(new Date().setTime(time));
  }

  /**
   *
   * @param value
   *
   */
  static isValidDateCurrent(value: string) {
    let dateObj = true;
    if (value !== '' || value !== null) {
      if (/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(value)) {
        dateObj = true;
      }
    }
    return dateObj;
  }

  /**
   *
   * @param value
   *
   */
  public static isValidDateBefore(value: string) {
    let dateObj = true;
    if (value !== '' && value !== null) {
      const today = new Date().setHours(23, 0, 0, 0);
      if (new Date(value) > new Date(today)) {
        dateObj = false;
      }
    }
    return dateObj;
  }

  /**
   *
   * @param value
   *
   */
  public static isValidDateAfter(value: string, c: FormControl) {

    const formGroup = <FormGroup>c.parent;
    let dateFin = null;
    if (formGroup) {
      dateFin = formGroup.controls.dateFin.value;
    }
    let dateObj = true;
    if (value !== '' || value !== null) {
      const today = new Date().setHours(23, 0, 0, 0);
      if (new Date(value) > new Date(dateFin)) {
        dateObj = false;
      }
    }
    return dateObj;
  }


  /**
   *
   * @param value
   *
   */
  public static isValidAvailabilityDater(value: string) {
    let dateObj = true;
    if (value !== '' || value !== null) {
      if (new Date(new Date().setHours(0, 0, 0, 0)) > new Date(value)) {
        dateObj = false;
      }
    }
    return dateObj;
  }

  /**
   *
   * @param value
   *
   */
  static isValidDate(value: string) {
    const dateObj = moment(value).format('DD/MM/YYYY');
    return dateObj !== 'Invalid date' ? true : false;
  }


  /**
   *
   * @param $data
   */
  static sortingFormationLifByDate($data) {
    $data = $data.sort(function (a, b) {
      return new Date(b.datefin).getTime() - new Date(a.datefin).getTime();
    });
    return $data;
  }
}
