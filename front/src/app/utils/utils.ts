
import {ErrorInfoMessage} from '../models/error-info-message';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

export class Utils {
    static INTERVAL_TIME_BTN_RESET = 5000;
    static DATE_BEFORE_CURRENT_DATE = 'La date doit être inférieure ou égale à la date du jour';
    static START_DATE_TO_VALIDATE = 'La date de début doit être inférieure ou égale à la date de fin';
    static END_GOBAL_DATE_TO_VALIDATE = 'La date doit être inférieure ou égale à la date du jour';
    static END_DATE_TO_VALIDATE = 'La date de fin doit être inférieure ou égale à la date du jour';
    static AVAILABILITY_DATE = 'La date de disponibilité doit être supérieure ou égale à la date du jour';

    static MASK_PHONE = {
        mask: [/0/, /[1-9]/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/],
        guide: false,
        keepCharPositions: true
    };

    static MASK_FINESS = {
        mask: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
        keepCharPositions: true,
        guide: false
    };

    static CAPATICITY_OF_INPUT = {
        mask: [/[1-9]/, /\d/, /\d/],
        keepCharPositions: true,
        guide: false
    };

    static MASK_DURATION = {
        placeholderChar: ' ',
        mask: [/[1-9]/, /\d/, /\d/, /\d/]
    };
    static MASK_INPUT_TYPE_NUMBER = {
        placeholderChar: ' ',
        mask: [/[1-2-3-4-5-6-7-8-9]/, /0/],
        guide: false
    };
    static MASK_DATE_FORMAT = {
        mask: [/[0-3]/, /[0-9]/, '/', /[0-1]/, /[0-9]/, '/', /[1-9]/, /[0-9]/, /[0-9]/, /[0-9]/]
    };

    static MASK_POSTAL_CODE = {
        mask: [/\d/, /\d/, /\d/, /\d/, /\d/],
        keepCharPositions: true,
        guide: false
    };
    static TIME_OUT_OF = [
        1000, 2000
    ];

    /**
     *
     * @returns {any}
     */
    static detectIE() {
        const ua = window.navigator.userAgent;
        const msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        const trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            const rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        const edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
        // other browser
        return false;
    }

    /**
     *
     * @param {Date} dateToString
     * @returns {string}
     */
    public static setDateToStringFormatServer(dateToString: Date) {
        const date = dateToString;
        let dateConvert = '';
        const mois=(date.getMonth()+1).toString().length>1?((date.getMonth() + 1)):('0' + (date.getMonth() + 1));
        dateConvert = date.getFullYear() + '-' + mois + '-' + ('0' + date.getDate()).slice(-2);
        return dateConvert;
    }

    /**
     *
     * @param {number} _start
     * @param {number} _stop
     * @param {number} _step
     * @returns {Array<number>}
     */
    public static range(_start: number, _stop?: number, _step?: number): Array<number> {
        let start: number = (_start) ? _start : 0;
        let stop: number = (_stop) ? _stop : _start;
        let step: number = (_step) ? _step : 1;

        let data: Array<number> = [];
        for (let i = start; i <= stop; i = i + step) {
            data.push(i);
        }
        return data;
    }

    /**
     *
     * @param value
     *
     */
    static isEmptyInput(value: string) {
        return value === '' || value === null ? true : false;
    }

    /**
     *
     * @param {string} cnt
     * @returns {string}
     */
    public static removeBlankContent(cnt: string) {
        let contValue = cnt;
        if (cnt && cnt !== undefined && cnt !== '') {
            contValue = cnt.split(' ').join('');
        }
        return contValue;
    }

    /**
     *
     * @param str
     * @returns {any}
     */
    public static strip_html_tags(str) {
        if ((str === null) || (str === ''))
            return false;
        else
            str = str.toString();
        return str.replace(/<[^>]*>/g, '');
    }

    /**
     *
     * @param {number} code
     * @param {string} calling
     * @returns {ErrorInfoMessage}
     */
    public static setErrorInfoMessage(code: number, calling: string) {
        const errorInfoStatus = new ErrorInfoMessage();
        switch (code) {
            case 0:
                errorInfoStatus.type = 'INFO';
                errorInfoStatus.code = code;
                errorInfoStatus.visible = true;
                errorInfoStatus.title = calling;
                break;
            case 1:
                errorInfoStatus.type = 'INFO';
                errorInfoStatus.code = code;
                errorInfoStatus.visible = true;
                errorInfoStatus.title = calling;
                break;
            case 204:
                errorInfoStatus.type = 'INFO';
                errorInfoStatus.code = code;
                errorInfoStatus.visible = true;
                errorInfoStatus.title = calling;
                break;
            case 400:
                errorInfoStatus.type = 'ERROR';
                errorInfoStatus.code = code;
                errorInfoStatus.visible = true;
                errorInfoStatus.title = 'ERROR';
                break;
            case 404:
                errorInfoStatus.type = 'INFO';
                errorInfoStatus.code = code;
                errorInfoStatus.visible = true;
                errorInfoStatus.title = calling;
                break;
            case 405:
                errorInfoStatus.type = 'ERROR';
                errorInfoStatus.code = code;
                errorInfoStatus.visible = true;
                errorInfoStatus.title = calling;
                break;
            case 500:
                errorInfoStatus.type = 'ERROR';
                errorInfoStatus.code = code;
                errorInfoStatus.visible = true;
                errorInfoStatus.title = calling;
                break;
            default:
                errorInfoStatus.visible = false;

        }
        return errorInfoStatus;
    }

    public static iOS() {

        const iDevices = [
            'iPad Simulator',
            'iPhone Simulator',
            'iPod Simulator',
            'iPad',
            'iPhone',
            'iPod'
        ];

        if (!!navigator.platform) {
            while (iDevices.length) {
                if (navigator.platform === iDevices.pop()) {
                    return true;
                }
            }
        }

        return false;
    }
    
    public static settingErrorLog(){
        return {
        provide: LocationStrategy,
        useClass: HashLocationStrategy
      }
    }
    
}
