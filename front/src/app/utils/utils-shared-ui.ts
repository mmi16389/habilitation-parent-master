export class UtilsPathConnexion {
  /**
   *
   * @param url
   */
  public static settingPathUrlConnexion(url: string) {

    setTimeout(() => {
      window.location.replace(url);
    });
    setTimeout(() => {
      window.location.reload();
    });
  }

  /**
   *
   * @param image
   */
  public createImageFromBlob(image: Blob) {
    let imageToShow = null;
    const reader = new FileReader();

    reader.addEventListener("load", () => {
      imageToShow = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
    return imageToShow;
  }

   /**
 * 
 * @param token 
 */
public static decodeTokenUser(token: string){
  console.log(' token to decode',token);
  const toArray = token.split('.');
  for(let i = 0; i < toArray.length - 1; i++) {
    localStorage.setItem('user', atob(toArray[i]));
  }
}
}
