/**
 *
 */


export class SortUtils {

    /**
     *
     */
    public static sortFile(data) {
        data = data.sort(function (a, b) {
            return a.libelle.localeCompare(b.libelle);
        });
        return data;
    }
}
