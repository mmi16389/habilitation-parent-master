import { BrowserModule } from '@angular/platform-browser';
import {ApplicationRef, NgModule} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {WorkflowAuthentificationModule} from './modules/workflow-authentification/workflow-authentification.module';
import {FooterContainerComponent} from './core/footer-container/footer-container.component';
import {GlobalContainerComponent} from './core/global-container/global-container.component';
import {HeadMenuComponent} from './core/head-menu/head-menu.component';
import {SideMenuComponent} from './core/side-menu/side-menu.component';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TextMaskModule} from 'angular2-text-mask';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule, LocationStrategy, HashLocationStrategy} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {WorkflowAphpAdminModule} from './modules/workflow-aphp-admin/workflow-aphp-admin.module';
import {WorkflowCrudUserAphpModule} from './modules/workflow-crud-user-aphp/workflow-crud-user-aphp.module';
import {BootstrapModalModule} from 'ng2-bootstrap-modal';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { WorkflowCategorieDomaineModule } from './modules/workflow-categorie-domaine/workflow-categorie-domaine.module';
import { WorkflowDomaineModule } from './modules/workflow-domaine/workflow-domaine.module';
import { ErrorInterceptor } from './handlers/error-hab-http-interceptor';
import { JwtInterceptor } from './handlers/jwt-hab-http-interceptor';
import { Utils } from './utils/utils';
// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    AppComponent,
    FooterContainerComponent,
    GlobalContainerComponent,
    HeadMenuComponent,
    SideMenuComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    WorkflowAuthentificationModule,
    WorkflowAphpAdminModule,
    WorkflowCrudUserAphpModule,
    WorkflowCategorieDomaineModule,
    WorkflowDomaineModule,
    BootstrapModalModule,
    NgbModule.forRoot(),
    TextMaskModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [ 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
    ,
    Utils.settingErrorLog()
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(applicationRef: ApplicationRef) {
    // for ng2-bootstrap-modal in angualar 5
    Object.defineProperty(applicationRef, '_rootComponents', {get: () => applicationRef['components']});
  }
}
