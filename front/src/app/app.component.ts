import {AfterViewChecked, Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {User} from './models/user';
import {Router} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewChecked {
  /**
   *
   */
  private lang = 'fr';
  /**
   *
   */
  public user: User;

  /**
   *
   * @param translate
   */
  public constructor(public translate: TranslateService, private router: Router) {
    localStorage.setItem('lang', this.lang);
  }

  /**
   *
   */
  ngOnInit() {
    if (localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      this.router.navigate(['aphp-login']);
    }
    this.translate.addLangs(['fr', 'en']);
  }


  /**
   *
   */
  ngAfterViewChecked(): void {
    this.lang = localStorage.getItem('lang');
    this.translate.setDefaultLang(this.lang);
  }
}
