import { Injectable } from '@angular/core';
import { AbstractService } from '../../../services/abstract.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategorieDomaineService extends AbstractService{

  /**
   * 
   * @param httpClient 
   */
  constructor(private httpClient: HttpClient) {
    super();
   }

   
  /**
   *
   */
  getListCategorie() {
    return this.httpClient.get(`${this.apiCategoriesDomaine}`);
  }

  /**
   * 
   * @param id 
   */
  getCategorieById(id: number) {
    return this.httpClient.get(`${this.apiCategoriesDomaine+'/'+id}`);
  }

   /**
   * 
   * @param id 
   */
  getDomaineByCategorieId(id: number) {
    return this.httpClient.get(`${this.apiCategoriesDomaine+'/'+id+'/domaines'}`);
  }
}
