import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCategorieDomaineComponent } from './components/list-categorie-domaine/list-categorie-domaine.component';

const routes: Routes = [
  {
    path: 'list-categorie-domaine',
    component: ListCategorieDomaineComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkflowCategorieDomaineRoutingModule { }
