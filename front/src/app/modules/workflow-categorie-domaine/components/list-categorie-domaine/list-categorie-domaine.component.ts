import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CategorieDomaineService } from '../../services/categorie-domaine.service';

@Component({
  selector: 'app-list-categorie-domaine',
  templateUrl: './list-categorie-domaine.component.html',
  styleUrls: ['./list-categorie-domaine.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListCategorieDomaineComponent implements OnInit {
  /**
   * 
   */
  rows: any;
  /**
   * 
   */
  public expanded: any = {};
  /**
   * 
   */
  public selected = [];
  /**
   * 
   */
  temp = [];
  /***
   * 
   */
  @ViewChild('myTable') table: DatatableComponent;

  /**
   * 
   * @param categoriDomainService 
   */
  constructor(private categoriDomainService: CategorieDomaineService) {
    this.rows = [];
  }
  /**
   * 
   */
  ngOnInit() {
    this.getListCategoriedomaine();
  }

  /**
    *
    * @param row
    */
  public getRowClass(row) {
    return {
      'ngx-cell': true,
      'ngx-cell-each-column': row.actif === 'FALSE'
    };
  }

  /**
   *
   * @param row
   * @param column
   * @param value
   */
  public getCellClass({ row, column, value }): any {
    return {
      'text-ngx-table': true,
    };
  }

  /**
   *
   * @param row
   */
  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  /**
   *
   * @param event
   */
  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  /**
   * 
   */
  getListCategoriedomaine() {
    this.categoriDomainService.getListCategorie().subscribe((data) => {
      this.rows = data;
    })
  }
  /**
   * 
   * @param obj 
   */
  checkStringOccurences(obj:any){
    const el=''+JSON.stringify(obj);
    return el.indexOf('categorieParent') ===-1?0:el.indexOf('categorieParent');
  }
}
