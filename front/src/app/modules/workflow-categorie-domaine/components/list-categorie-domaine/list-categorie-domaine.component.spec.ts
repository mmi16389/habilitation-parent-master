import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCategorieDomaineComponent } from './list-categorie-domaine.component';

describe('ListCategorieDomaineComponent', () => {
  let component: ListCategorieDomaineComponent;
  let fixture: ComponentFixture<ListCategorieDomaineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCategorieDomaineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCategorieDomaineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
