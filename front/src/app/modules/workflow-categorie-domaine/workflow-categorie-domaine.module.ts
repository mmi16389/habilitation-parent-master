import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCategorieDomaineComponent } from './components/list-categorie-domaine/list-categorie-domaine.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { WorkflowCategorieDomaineRoutingModule } from './workflow-categorie-domaine-routing.module';

@NgModule({
  declarations: [
    ListCategorieDomaineComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgSlimScrollModule,
    NgbModule.forRoot(),
    NgxDatatableModule,
    WorkflowCategorieDomaineRoutingModule
  ]
})
export class WorkflowCategorieDomaineModule { }
