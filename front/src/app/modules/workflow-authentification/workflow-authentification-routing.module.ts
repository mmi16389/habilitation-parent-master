import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserLoginComponent} from './user-login/user-login.component';
import {UserLogoutComponent} from './user-logout/user-logout.component';
import {UrlConnexion} from '../../../assets/url-context/url-connexion';

const routes: Routes = [
  {
    path: UrlConnexion.LOGIN,
    component: UserLoginComponent
  },
  {
    path: UrlConnexion.LOGOUT,
    component: UserLogoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkflowAuthentificationRoutingModule {
}
