import {Component, OnInit} from '@angular/core';
import { UrlConnexion } from '../../../../assets/url-context/url-connexion';

@Component({
  selector: 'app-user-logout',
  templateUrl: './user-logout.component.html',
  styleUrls: ['./user-logout.component.scss']
})
export class UserLogoutComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    localStorage.clear();
    setTimeout(()=>{
      location.reload();
    })
  }

}
