import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {WorkflowAuthentificationRoutingModule} from './workflow-authentification-routing.module';
import {UserLoginComponent} from './user-login/user-login.component';
import {UserLogoutComponent} from './user-logout/user-logout.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {HttpLoaderFactory, WorkflowSharedModule} from '../workflow-shared/workflow-shared.module';

@NgModule({
  declarations: [
    UserLoginComponent,
    UserLogoutComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    WorkflowSharedModule,
    WorkflowAuthentificationRoutingModule
  ]
})
export class WorkflowAuthentificationModule {
}
