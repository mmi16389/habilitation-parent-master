import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/user';
import {FormControl, FormGroup} from '@angular/forms';
import {ShowErrorsInputFieldComponent} from '../../workflow-shared/show-errors-input-field/show-errors-input-field.component';
import {Router} from '@angular/router';
import {UtilsPathConnexion} from '../../../utils/utils-shared-ui';
import {UrlAppContext} from '../../../../assets/url-context/url-app-context';
import {AuthentificationService} from '../../../services/authentification.service';
import {Utilisateur} from './../../../models/utilisateur';


@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {
  /**
   *
   */
  public user: User = null;
  /**
   *
   */
  public loginForm: FormGroup = new FormGroup({});

  /**
   *
   */
  public hasError: boolean;

  /**
   *
   */
  public detailsSpinner = true;

  /**
   *
   * @param router
   * @param apiAuthentification
   */
  constructor(
    private router: Router,
    private apiAuthentification: AuthentificationService) {
  }

  ngOnInit() {
    this.initLoginForm();
    if (localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
      this.router.navigate(['home-page']);
    }
  }

  initLoginForm() {
    this.loginForm = new FormGroup({
      'login': new FormControl(null),
      'password': new FormControl(null)
    })
    ;
  }

  /**
   *
   * @param $element
   */
  setLanguage($element) {
    localStorage.setItem('lang', $element);
  }

  /**
   *
   * @param element
   */
  onSubmit(element) {

    if (element.valid) {
      this.checkLogin(this.loginForm.get('login').value, this.loginForm.get('password').value);
    } else if (!element.valid) {
      let objForm = new ShowErrorsInputFieldComponent();
      objForm.notValideForm(element);
      objForm = null;
    }
  }

  /**
   *
   * @param login
   * @param pass
   */
  checkLogin(login: string, pass: string) {
    this.detailsSpinner = false;
    let checkLogin = false; 

    this.apiAuthentification.loginJWT(login, pass).subscribe((data) => {
      localStorage.setItem('token', data.token);
      UtilsPathConnexion.decodeTokenUser(data.token);
      checkLogin = true;
      this.detailsSpinner = true;
      UtilsPathConnexion.settingPathUrlConnexion(UrlAppContext.HOME_PAGE_HASHING);
    }, error => {
      this.hasError = true;
      this.detailsSpinner = true;
    });
    return checkLogin;
  }

  /**
   *
   * @param $event
   */
  detectEvent($event) {
    this.hasError = false;
  }
}
