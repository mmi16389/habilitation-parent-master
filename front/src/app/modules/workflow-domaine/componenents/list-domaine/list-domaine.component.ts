import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DomaineService } from '../../services/domaine.service';

@Component({
  selector: 'app-list-domaine',
  templateUrl: './list-domaine.component.html',
  styleUrls: ['./list-domaine.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListDomaineComponent implements OnInit {
  /**
   * 
   */
  rows: any;
  /**
   * 
   */
  public expanded: any = {};
  /**
   * 
   */
  public selected = [];
  /**
   * 
   */
  temp = [];
  /***
   * 
   */
  @ViewChild('myTable') table: DatatableComponent;

  /**
   * 
   * @param domaineService 
   */
  constructor(private domaineService: DomaineService) {

  }

  ngOnInit() {
    this.getListDomaine();
  }

  /**
    *
    * @param row
    */
  public getRowClass(row) {
    return {
      'ngx-cell': true,
      'ngx-cell-each-column': row.actif === 'FALSE'
    };
  }

  /**
   *
   * @param row
   * @param column
   * @param value
   */
  public getCellClass({ row, column, value }): any {
    return {
      'text-ngx-table': true,
    };
  }

  /**
   *
   * @param row
   */
  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  /**
   *
   * @param event
   */
  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }
  /**
   * 
   */
  getListDomaine() {
    this.domaineService.getListDomaines().subscribe((data) => {
      this.rows = data;
    })
  }
}
