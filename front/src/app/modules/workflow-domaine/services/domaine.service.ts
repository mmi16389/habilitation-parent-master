import { Injectable } from '@angular/core';
import { AbstractService } from '../../../services/abstract.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DomaineService extends AbstractService {

  /**
   * 
   * @param httpClient 
   */
  constructor(private httpClient: HttpClient) {
    super();
  }

  /**
    *
    * @param iduser
    */
  getListDomaines() {
    return this.httpClient.get(`${this.apiDomaine}`);
  }

}
