import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkflowDomaineRoutingModule } from './workflow-domaine-routing.module';
import { ListDomaineComponent } from './componenents/list-domaine/list-domaine.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [ListDomaineComponent],
  imports: [
      CommonModule,
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      NgxDatatableModule,
      NgSlimScrollModule,
      NgbModule.forRoot(),
      NgxDatatableModule,
    WorkflowDomaineRoutingModule
  ]
})
export class WorkflowDomaineModule { }
