import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListDomaineComponent } from './componenents/list-domaine/list-domaine.component';

const routes: Routes = [
  {
    path: 'list-domaine',
    component: ListDomaineComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkflowDomaineRoutingModule { }
