import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomizeDialogueModalComponent } from './customize-dialogue-modal.component';

describe('CustomizeDialogueModalComponent', () => {
  let component: CustomizeDialogueModalComponent;
  let fixture: ComponentFixture<CustomizeDialogueModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomizeDialogueModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomizeDialogueModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
