import {Component, OnInit} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';

export interface ConfirmModal {
  title: string;
  action: any;
}

@Component({
  selector: 'app-confirm-dialogue-modal',
  templateUrl: './customize-dialogue-modal.component.html',
  styleUrls: ['./customize-dialogue-modal.component.scss']
})
export class CustomizeDialogueModalComponent extends DialogComponent<ConfirmModal, boolean> implements OnInit {
  public title: string;
  public action: any;
  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  ngOnInit() {
  }

  _activated() {
    this.result = true;
    this.close();
  }
}
