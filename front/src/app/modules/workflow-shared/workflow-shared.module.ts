import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {ValidatorEmptyFieldDirective} from '../../directives/validators/validator-empty-field/validator-empty-field.directive';
import {ShowErrorsInputFieldComponent} from './show-errors-input-field/show-errors-input-field.component';
import {CustomCalendarDirective} from '../../directives/validators/custom-calendar-validator/custom-calendar.directive';
import {ValidatorCalendarDirective} from '../../directives/validators/validator-calendar/validator-calendar.directive';
import {ErrorAlertFieldComponent} from './error-alert-field/error-alert-field.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxMyDatePickerModule} from 'ngx-mydatepicker';
import {TextMaskModule} from 'angular2-text-mask';
import {PopoverModule} from 'ngx-bootstrap';
import {ValidatorCompareDirective} from '../../directives/validators/validator-compare-sizeof/validator-compare.directive';
import {AphpDatepickerComponent} from './aphp-datepicker/aphp-datepicker.component';
import {DateAppPipe} from '../../pipes/pipe-date/date-app.pipe';
import { ConfirmDialogueModalComponent } from './confirm-dialogue-modal/confirm-dialogue-modal.component';
import {CustomizeDialogueModalComponent} from "./custumize-dialogue-modal/customize-dialogue-modal.component";


// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  imports: [
    CommonModule,
    PopoverModule,
    NgbModule.forRoot(),
    TextMaskModule,
    NgxMyDatePickerModule.forRoot(),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    ValidatorEmptyFieldDirective,
    ShowErrorsInputFieldComponent,
    CustomCalendarDirective,
    ValidatorCalendarDirective,
    ValidatorCompareDirective,
    ErrorAlertFieldComponent,
    AphpDatepickerComponent,
    DateAppPipe,
    ConfirmDialogueModalComponent, CustomizeDialogueModalComponent
  ],
  exports: [
    ErrorAlertFieldComponent,
    ValidatorEmptyFieldDirective,
    ShowErrorsInputFieldComponent,
    CustomCalendarDirective,
    ValidatorCalendarDirective,
    ValidatorCompareDirective,
    AphpDatepickerComponent,
    ConfirmDialogueModalComponent,
      CustomizeDialogueModalComponent,
    DateAppPipe
  ],
  entryComponents: [ConfirmDialogueModalComponent, CustomizeDialogueModalComponent],
})
export class WorkflowSharedModule {
}
