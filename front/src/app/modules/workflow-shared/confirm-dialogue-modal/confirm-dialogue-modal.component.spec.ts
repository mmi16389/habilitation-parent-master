import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDialogueModalComponent } from './confirm-dialogue-modal.component';

describe('ConfirmDialogueModalComponent', () => {
  let component: ConfirmDialogueModalComponent;
  let fixture: ComponentFixture<ConfirmDialogueModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmDialogueModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDialogueModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
