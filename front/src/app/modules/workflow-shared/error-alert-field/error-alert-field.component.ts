import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {ErrorInfoMessage} from '../../../models/error-info-message';
import {TranslateLabelService} from '../../../services/translate.service';

@Component({
    selector: 'app-error-alert-field',
    templateUrl: './error-alert-field.component.html',
    styleUrls: ['./error-alert-field.component.scss']
})
export class ErrorAlertFieldComponent implements OnInit, OnChanges {

    @Input()
    public errorInfoStatus: ErrorInfoMessage;
    public title: string;
    public message: string;

    constructor(private translateLabelService: TranslateLabelService) {
        this.errorInfoStatus = new ErrorInfoMessage();
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.errorInfoStatus.currentValue) {
            if (changes.errorInfoStatus.currentValue.title) {
                // this.translateLabelService.getLableforI18n(changes.errorInfoStatus.currentValue.title, '').subscribe((dta) => {
                //     this.title = dta[changes.errorInfoStatus.currentValue.code];
                // });
            }
        }
    }
}
