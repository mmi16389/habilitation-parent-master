import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AlertFieldComponent} from './alert-indicator.component';

describe('ErrorAlertFieldComponent', () => {
    let component: AlertFieldComponent;
    let fixture: ComponentFixture<AlertFieldComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AlertFieldComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AlertFieldComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
