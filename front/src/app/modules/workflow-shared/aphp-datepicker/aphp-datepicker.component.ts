import {
    Component, OnInit,
    forwardRef, ViewChild, Inject,
    ElementRef, Input, OnChanges, Renderer, SimpleChanges, AfterViewChecked
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl} from '@angular/forms';
import {INgxMyDpOptions, NgxMyDatePickerDirective, IMyDateModel} from 'ngx-mydatepicker';

import * as moment from 'moment';


@Component({
    selector: 'app-aphp-datepicker',
    templateUrl: './aphp-datepicker.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AphpDatepickerComponent),
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useExisting: forwardRef(() => AphpDatepickerComponent),
            multi: true
        }
    ],
    styleUrls: ['./aphp-datepicker.component.scss'],
})

export class AphpDatepickerComponent implements ControlValueAccessor, OnInit, OnChanges, AfterViewChecked {
    /**
     *
     */
    @ViewChild('dp') public ngxdp: NgxMyDatePickerDirective;
    /**
     *
     */
    @Input() public ngModelFormControl: any;
    /**
     *
     * @type {boolean}
     */
    @Input() public editable = true;
    /**
     *
     */
    @Input() public error: boolean;
    /**
     *
     */
    @Input() public reactiveFormControl: any;
    /**
     *
     */
    @Input() public btnReset: boolean;
    /**
     *
     * @type {boolean}
     */
    @Input() public stateInputDatePicker = false;
    /**
     *
     * @type {string}
     */
    @Input() public datePickerErrorMessage = 'Le format de la date n\'est pas correct';
    /**
     *
     * @type {boolean}
     */
    public isValide = false;
    /**
     *
     * @type {boolean}
     */
    public hideClearCalendar = true;
    /**
     *
     * @type {{mask: (RegExp | string)[]; keepCharPositions: boolean; guide: boolean}}
     */
    public MASK_DATE_FORMAT = {
        mask: [/[0-3]/, /[0-9]/, '/',
            /[0-1]/, /[0-9]/, '/', /[1-2]/,
            /[0-9]/, /[0-9]/, /[0-9]/],
        keepCharPositions: true,
        guide: false
    };
    /**
     *
     * @type {{dateFormat: string; todayBtnTxt: string; dayLabels: {su: string; mo: string; tu: string; we: string; th: string; fr: string; sa: string}; monthLabels: {"1": string; "2": string; "3": string; "4": string; "5": string; "6": string; "7": string; "8": string; "9": string; "10": string; "11": string; "12": string}}}
     */
    public datePickerOptions: INgxMyDpOptions = {
        dateFormat: 'dd/mm/yyyy',
        todayBtnTxt: 'Aujourd\'hui',
        dayLabels: {su: 'Dim', mo: 'Lun', tu: 'Mar', we: 'Mer', th: 'Jeu', fr: 'Ven', sa: 'Sam'},
        monthLabels: {
            1: 'Jan', 2: 'Fév', 3: 'Mar', 4: 'Avr',
            5: 'Mai', 6: 'Juin', 7: 'Juil', 8: 'Aoû',
            9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Déc'
        }
    };
    /**
     *
     */
    public model: string;
    /**
     *
     */
    private control: FormControl;
    /**
     *
     * @param _
     */
    private propagateChange = (_: any) => {
    };

    /**
     *
     * @param {Renderer} renderer
     * @param {ElementRef} elRef
     */
    constructor(@Inject(Renderer) private renderer: Renderer, private elRef: ElementRef) {
    }

    /**
     *
     */
    ngOnInit(): void {
        this.onChangeReadonly();
    }

    /**
     *
     * @param {FormControl} c
     */
    validate(c: FormControl) {
        this.control = c;
        c.statusChanges.subscribe((dataStatus) => {
            if (dataStatus === 'VALID') {
                this.error = false;
            }
        });
    }

    /**
     *
     * @param {SimpleChanges} changes
     */
    ngOnChanges(changes: SimpleChanges): void {
        this.onChangeReadonly();
    }

    /**
     *
     */
    ngAfterViewChecked(): void {
        this.onChangeReadonly();
    }

    /**
     *
     * @param {IMyDateModel} event
     */
    onDateChanged(event: IMyDateModel) {
        if (event.date.year !== 0) {
            this.hideClearCalendar = false;
            this.isValide = false;
            const nm = Number(event.date.month);
            const nd = Number(event.date.day);
            const mois = nm <= 9 ? 0 + '' + nm : nm;
            const day = nd <= 9 ? 0 + '' + nd : nd;
            this.propagateChange(event.date.year + '-' + mois + '-' + day);
            this.model = day + '/' + mois + '/' + event.date.year;
        } else {
            this.propagateChange(null);
            this.model = null;
        }
    }

    /**
     *
     * @param $event
     */
    onInputFieldChanged($event) {
        this.isValide = true;
        this.hideClearCalendar = false;

        if (this.control) {
            if (!this.ngxdp.isDateValid()) {
                //this.control.setErrors({'datepicker': {'message': this.datePickerErrorMessage}});
            }
            if (this.ngxdp.isDateValid()) {
                this.isValide = false;
                this.control.updateValueAndValidity();
            }
        }
    }

    /**
     *
     * @param obj
     */
    writeValue(obj: any): void {
        if (obj !== null) {
            this.hideClearCalendar = false;
            const newObj = moment(obj, 'YYYY-MM-DD').format('DD/MM/YYYY');
            this.model = newObj === 'Invalid date' ? null : newObj;
        } else {
            this.model = null;
        }
    }

    /**
     *
     * @param fn
     */
    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    /**
     *
     * @param fn
     */
    registerOnTouched(fn: any): void {
    }

    /**
     *
     */
    onChangeReadonly() {
        const enabled = (!this.elRef.nativeElement.attributes.readonly) && this.editable;
        this.renderer.setElementProperty(this.elRef.nativeElement, 'readonly', !enabled);
        this.ngxdp.setDisabledState(!enabled);
    }

    /**
     *
     * @param {boolean} isDisabled
     */
    setDisabledState?(isDisabled: boolean): void {
        this.renderer.setElementProperty(this.elRef.nativeElement, 'readonly', isDisabled);
        this.ngxdp.setDisabledState(isDisabled);
    }

    /**
     *
     * @param {boolean} error
     */
    setErrorMessage(error: boolean) {
        this.error = error;
    }
}
