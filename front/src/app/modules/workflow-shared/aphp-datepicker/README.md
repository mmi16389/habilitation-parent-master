# aphp-datepicker
 
## Description
Angular component date picker. Compatible __Angular2+__.

## Installation && dependencies
# before using the component : open-groupe-datepicker; 
# be sure, thoses following plugins were installed,
# for each one, then import it into your @NgModule:

1- ngx-mydatepicker
__npm install ngx-mydatepicker --save__
## Usage
Follow instructions from [here](https://github.com/kekeh/mydatepicker)

2- angular2-text-mask 
__npm i angular2-text-mask --save__

## Usage
Follow instructions from [here](https://www.npmjs.com/package/angular2-text-mask)
 
## Samples

Stand-alone
  <app-open-groupe-datepicker [ngModel]="model"></app-open-groupe-datepicker>

Simple form

<form #myForm="ngForm" (ngSubmit)="myForm.value">
  ...
  <app-open-groupe-datepicker name="mydate" ngModel></app-open-groupe-datepicker>
  ...
</form>


Reactive Form
<form [formGroup]"myForm" (ngSubmit)="myForm.value">
  ...
  <app-open-groupe-datepicker formControlName="openingDate"></app-open-groupe-datepicker>
  ...
</form>

## Compatibility (tested with)
* Firefox (latest)
* Chrome (latest)
* Chromium (latest)
* Edge
* IE11
* Safari

## License
* License: MIT

## Author
* Author: MMI16389
