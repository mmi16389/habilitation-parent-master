import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AphpDatepickerComponent } from './aphp-datepicker.component';

describe('AphpDatepickerComponent', () => {
  let component: AphpDatepickerComponent;
  let fixture: ComponentFixture<AphpDatepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AphpDatepickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AphpDatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
