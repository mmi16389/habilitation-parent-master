import {Component, Input, OnInit} from '@angular/core';
import {
    AbstractControlDirective,
    AbstractControl,
    FormGroup,
    FormControl,
    FormArray
} from '@angular/forms';
import {} from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'app-show-errors-input-field',
    templateUrl: './show-errors-input-field.component.html',
    styleUrls: ['./show-errors-input-field.component.scss']
})
export class ShowErrorsInputFieldComponent implements OnInit {

    private static readonly errorMessages = {
        'required': () => '',
        'emptyField': (params) => params.message,
        'email': (params) => params.message,
        'calendar': (params) => params.message,
        'datepicker': (params) => params.message,
    };
    @Input()
    private control: AbstractControlDirective | AbstractControl;
    @Input()
    private elementRefError: any;
    @Input()
    private typeMessage: string;

    ngOnInit(): void {
    }

    shouldShowErrors(): boolean {
        return this.control &&
            this.control.errors &&
            (this.control.dirty || this.control.touched);
    }

    listOfErrors(): string[] {
        return Object.keys(this.control.errors)
            .map(field => this.getMessage(field, this.control.errors[field]));
    }

    /**
     *
     * @param type
     * @param params
     *
     */
    private getMessage(type: string, params: any) {
        return ShowErrorsInputFieldComponent.errorMessages[type](params);
    }

    public notValideForm(elementForm: FormGroup) {
        Object.keys(elementForm.controls).map((field) => {
            elementForm.controls[field].markAsTouched();
            if (elementForm.controls[field] instanceof FormControl) {
                elementForm.controls[field].markAsTouched();
            }
            else if (elementForm.controls[field] instanceof FormGroup) {
                const objControl = <FormGroup>elementForm.controls[field]['controls'];
                Object.keys(objControl).map((_field) => {
                    objControl[_field].markAsTouched();
                });
            }
            else if (elementForm.controls[field] instanceof FormArray) {
                const objControl = <FormGroup>elementForm.controls[field]['controls'];
                Object.keys(objControl).map((_field) => {
                    objControl[_field].markAsTouched();
                });
            }
        });
    }
}
