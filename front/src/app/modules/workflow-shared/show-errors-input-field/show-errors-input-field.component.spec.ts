import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ShowErrorsInputFieldComponent} from './show-errors-input-field.component';

describe('ShowErrorsInputFieldComponent', () => {
    let component: ShowErrorsInputFieldComponent;
    let fixture: ComponentFixture<ShowErrorsInputFieldComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ShowErrorsInputFieldComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ShowErrorsInputFieldComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
