import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbstractSharedComponent } from './abstract-shared.component';

describe('AbstractSharedComponent', () => {
  let component: AbstractSharedComponent;
  let fixture: ComponentFixture<AbstractSharedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbstractSharedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractSharedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
