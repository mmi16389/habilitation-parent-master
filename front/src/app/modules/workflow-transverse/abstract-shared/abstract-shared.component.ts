import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-abstract-shared',
  templateUrl: './abstract-shared.component.html',
  styleUrls: ['./abstract-shared.component.scss']
})
export class AbstractSharedComponent implements OnInit {

  /**
   *
   * @param apiService
   */
  constructor(
    public _location: Location) {
  }

  ngOnInit() {
  }

  /**
   *
   */
  public goBack() {
    this._location.back();
  }

}
