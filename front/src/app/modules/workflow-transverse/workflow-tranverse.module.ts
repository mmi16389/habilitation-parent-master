import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {routes} from './workflow-tranverse-routing.module';
import { AbstractSharedComponent } from './abstract-shared/abstract-shared.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild(
      routes
    ),
  ],
  declarations: [AbstractSharedComponent],
  exports: [],
  providers: []
})
export class WorkflowTranverseModule {
}
