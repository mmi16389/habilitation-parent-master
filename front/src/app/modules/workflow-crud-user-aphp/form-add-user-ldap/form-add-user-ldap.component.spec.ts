import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAddUserLdapComponent } from './form-add-user-ldap.component';

describe('FormAddUserLdapComponent', () => {
  let component: FormAddUserLdapComponent;
  let fixture: ComponentFixture<FormAddUserLdapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAddUserLdapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAddUserLdapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
