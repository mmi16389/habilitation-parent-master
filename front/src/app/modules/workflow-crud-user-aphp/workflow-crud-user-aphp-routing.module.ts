import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WorkflowCrudUserPageComponent} from './workflow-crud-user-page/workflow-crud-user-page.component';
import {ListUserAdminComponent} from './list-user-admin/list-user-admin.component';
import {ConfirmUserLdapComponent} from './confirm-user-ldap/confirm-user-ldap.component';
import {FormAddUserLdapComponent} from './form-add-user-ldap/form-add-user-ldap.component';
import {GererHabilitationsUserComponent} from './gerer-habilitations-user/gerer-habilitations-user.component';

const routes: Routes = [
  {
    path: 'add-user-ldap',
    component: WorkflowCrudUserPageComponent
  },
  {
    path: 'list-user',
    component: ListUserAdminComponent
  },
  {
    path: 'confirm-user-ldap',
    component: ConfirmUserLdapComponent
  },
  {
    path: 'modal-user-dialogue',
    component: FormAddUserLdapComponent
  },
  {
    path: 'habilitations-user/:id/details',
    component: GererHabilitationsUserComponent
  },
  {
    path: 'habilitations-user',
    component: GererHabilitationsUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkflowCrudUserAphpRoutingModule {
}
