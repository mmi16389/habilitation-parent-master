import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-confirm-user-ldap',
  templateUrl: './confirm-user-ldap.component.html',
  styleUrls: ['./confirm-user-ldap.component.scss']
})
export class ConfirmUserLdapComponent implements OnInit {
  public userLDAP: any;

  constructor(private route: ActivatedRoute,
              private router: Router) {

    this.route.params.subscribe((params: any) => {
      this.userLDAP = JSON.parse(params['userLdap']);
    });
  }

  ngOnInit() {
    console.log(' this user ', this.userLDAP);
  }

}
