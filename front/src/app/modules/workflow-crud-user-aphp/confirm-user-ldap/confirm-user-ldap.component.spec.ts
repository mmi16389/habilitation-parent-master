import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmUserLdapComponent } from './confirm-user-ldap.component';

describe('ConfirmUserLdapComponent', () => {
  let component: ConfirmUserLdapComponent;
  let fixture: ComponentFixture<ConfirmUserLdapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmUserLdapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmUserLdapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
