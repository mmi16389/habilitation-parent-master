import { Component, OnInit } from '@angular/core';
import { ShowErrorsInputFieldComponent } from '../../workflow-shared/show-errors-input-field/show-errors-input-field.component';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { CrudUserAphpService } from '../../../services/crud-user-aphp.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractSharedComponent } from '../../workflow-transverse/abstract-shared/abstract-shared.component';
import { Location } from '@angular/common';
import { HabilitationsUserService } from 'src/app/services/habilitations-user.service';  
import { CategorieDomaineService } from '../../workflow-categorie-domaine/services/categorie-domaine.service';

@Component({
  selector: 'app-gerer-habilitations-user',
  templateUrl: './gerer-habilitations-user.component.html',
  styleUrls: ['./gerer-habilitations-user.component.scss']
})
export class GererHabilitationsUserComponent extends AbstractSharedComponent implements OnInit {

  /**
   *
   */
  public states = [];

  /**
   *
   */
  public groups = [];
  /**
   *
   */
  public sites = [];
  /**
   *
   */
  public gpCheccked = false;

  /**
   *
   */
  public addUserLDAPForm: FormGroup = new FormGroup({});
  /**
   *
   */
  public codeAphUser: number;

  /**
   *
   */
  public userLDAP: any;
  /**
   *
   */
  public categories: any;
  /**
   *
   */
  public domaines: any;
  /**
   *
   */
  public groupes = [];

  /**
   *
   */
  public listGrByUSer = [];


  /**
   *
   */
  public checkedMap = new Map();
  /**
   *
   */
  private respo: Array<any> = [];

  /**
   *
   */
  public firstDeleteGr = true;

  /**
   *
   * @param apiService
   */
  constructor(
    private apiService: CrudUserAphpService,
    private categoriDomainService: CategorieDomaineService,
    private habilitationsService:HabilitationsUserService,
    private router: Router, private route: ActivatedRoute, public _location: Location) {
    super(_location);
    this.route.params.subscribe((params: any) => {
      if (params) {
        this.codeAphUser = params.id;
        this.getGroupByUSer(this.codeAphUser, 'RTH');
        this.apiService.getUserById(this.codeAphUser).subscribe((data: any) => {
          this.userLDAP = data;
        });
      }
    });
  }

  ngOnInit() {
    this.getUserLDAP();
    this.getGroupsAPHP();
    this.getSiteAPHP();
    this.getListCategorie();
    this.initializeForm();

    /**
     *
     */
    this.addUserLDAPForm.get('categorie').valueChanges.subscribe((data) => {
      this.categoriDomainService.getDomaineByCategorieId(data).subscribe((data) => {
        this.domaines = data;
      })
    });

    this.addUserLDAPForm.get('domaine').valueChanges.subscribe((data) => {
      // let domaine = null;
      // domaine = this.domaines.filter((el: any) => el.id === Number(data));
      // this.groupes = domaine[0].groupe;
    });
  }

  /**
   *Initialiser le formulaire
   */
  private initializeForm() {
    this.addUserLDAPForm = new FormGroup({
      'categorie': new FormControl(null),
      'domaine': new FormControl(null),
      'groupes': new FormArray([])
    })
      ;
  }

  /**
   *
   */
  getUserLDAP() {
    this.apiService.getUserLDAP().subscribe((data: any) => {
      this.states = data;
    });
  }

  /**
   *
   */
  getGroupsAPHP() {
    this.habilitationsService.getGroupsAPHP().subscribe((dta: any) => {
      this.groups = dta;
    });
  }

  /**
   *
   */
  getSiteAPHP() {
    this.habilitationsService.getSiteAPHP().subscribe((dta: any) => {
      this.sites = dta;
    });
  }

  /**
   *
   * @param {FormGroup} element
   */
  onSubmit(element: FormGroup) {
    if (element.valid) {
    } else if (!element.valid) {
      let objForm = new ShowErrorsInputFieldComponent();
      const gp = this.addUserLDAPForm.controls['groupes']['controls'];
      if (gp.toString().length === 0) {
        this.gpCheccked = true;
      }
      objForm.notValideForm(element);
      objForm = null;
    }
  }


  getListCategorie() {
    this.categoriDomainService.getListCategorie().subscribe((data) => {
      this.categories = data;
    });
  }

  /**
   * 
   * @param id 
   * @param domain 
   */
  getGroupByUSer(id: number, domain: string) {
    this.apiService.getGroupUserById(id, domain).subscribe((data: any) => {
      this.listGrByUSer = data;
      sessionStorage.setItem('listGroup', JSON.stringify(this.listGrByUSer));
      this.listGrByUSer.forEach((el: any) => {
        this.respo.push(el.id);
      });
    });
  }


  /**
   *
   * @param id
   * @param $event
   */
  onSelectedGroupes(group, $event) {
    if ($event) {
      this.respo.push(group.id);
      this.checkedMap.set(group.id, true);
      this.listGrByUSer.push(group);
    } else {
      this.respo.forEach((item, index) => {
        if (item === group.id) {
          this.respo.splice(index, 1);
          this.checkedMap.set(group.id, false);
        }
      });
      this.listGrByUSer.forEach((item, index) => {
        if (item.id === group.id) {
          this.listGrByUSer.splice(index, 1);
        }
      });
    }
    console.log(' LA MAP ', this.checkedMap, ' LA response envoye : ', this.respo);
  }

  /**
   *
   * @param idGroup
   */
  checkGroupSelectToListGroup(idGroup: number) {
    let check = false;
    let val = null;
    val = this.groupes.filter((el: any) => el.id === idGroup);
    if (val.length && val.length > 0) {
      check = true;
    } else {
      check = false;
    }
    return check;
  }

  /**
   *
   * @param idGroup
   */
  checkGroupListGroupToSelect(idGroup: number) {
    let check = false;
    let val = null;
    val = this.listGrByUSer.filter((el: any) => el.id === idGroup);

    if (val.length && val.length > 0) {
      check = true;
    } else {
      check = false;
    }
    return check;
  }

  /**
   *
   * @param idGroup
   */
  checkGroupListGroupTempToSelect(idGroup: number) {
    let check = false;
    let val = null;
    let listGr = [];
    if (sessionStorage.getItem('listGroup')) {
      listGr = JSON.parse(sessionStorage.getItem('listGroup'));
      val = listGr.filter((el: any) => el.id === idGroup);

      if (val.length && val.length > 0) {
        check = true;
      } else {
        check = false;
      }
    }
    return check;
  }

  /**
   *
   * @param group
   */
  deleteGroupInstance(group) {
    this.listGrByUSer.forEach((item, index) => {
      if (item.id === group.id) {
        this.listGrByUSer.splice(index, 1);
        sessionStorage.setItem('listGroup', JSON.stringify(this.listGrByUSer));
      }
    });
  }
}
