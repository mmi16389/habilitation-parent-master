import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GererHabilitationsUserComponent } from './gerer-habilitations-user.component';

describe('GererHabilitationsUserComponent', () => {
  let component: GererHabilitationsUserComponent;
  let fixture: ComponentFixture<GererHabilitationsUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GererHabilitationsUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GererHabilitationsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
