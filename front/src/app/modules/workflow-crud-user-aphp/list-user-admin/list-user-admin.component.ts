import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {CrudUserAphpService} from '../../../services/crud-user-aphp.service';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {DialogService} from 'ng2-bootstrap-modal';
import {FormAddUserLdapComponent} from '../form-add-user-ldap/form-add-user-ldap.component';
import {ConfirmDialogueModalComponent} from '../../workflow-shared/confirm-dialogue-modal/confirm-dialogue-modal.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list-user-admin',
  templateUrl: './list-user-admin.component.html',
  styleUrls: ['./list-user-admin.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListUserAdminComponent implements OnInit {

  rows = [];
  public expanded: any = {};
  public selected = [];
  temp = [];
  @ViewChild('myTable') table: DatatableComponent;


  /**
   *
   * @param serviceUser
   * @param dialogService
   */
  constructor(private serviceUser: CrudUserAphpService,
              private dialogService: DialogService, private router: Router) {

  }

  ngOnInit() {
    this.getListUser();
  }

  /**
   *
   * @param row
   */
  public getRowClass(row) {
    return {
      'ngx-cell': true,
      'ngx-cell-each-column': row.actif === 'FALSE'
    };
  }

  /**
   *
   * @param row
   * @param column
   * @param value
   */
  public getCellClass({row, column, value}): any {
    return {
      'text-ngx-table': true,
    };
  }

  /**
   *
   * @param row
   */
  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  /**
   *
   * @param event
   */
  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  /**
   *
   * @param row
   */
  deleteUser(row) {
    this.serviceUser.deleteUser(row.id).subscribe((data) => {
      this.serviceUser.getListUser().subscribe((element: any) => {
        this.rows = element;
      });
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.nomUsuel.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  /** 
   *
   */
  addNewUSer() {
    this.dialogService.addDialog(FormAddUserLdapComponent, {
      title: 'Ajouter un nouveau utilisateur',
      action: '',

    }).subscribe((response) => {
      if (response) {
        this.getListUser();
      }
    });
  }

  /**
   *
   */
  getListUser() {
    this.serviceUser.getListUser().subscribe((data: any) => {
      this.temp = [...data];
      this.rows = data;
    });
  }

  /**
   *
   * @param row
   */
  confirm(row) {

    let title = '';
    let label = '';
    if (row.actif === '1') {
      title = 'Souhaitez-vous desactiver le compte de : ' + row.nomUsuel + ' ' + row.prenom + ' ?';
      label = 'Confirmation de desactivation';
    }
    if (row.actif === '0') {
      title = 'Souhaitez-vous activer le compte de : ' + row.nomUsuel + ' ' + row.prenom + ' ?';
      label = 'Confirmation d\'activation';
    }
    this.dialogService.addDialog(ConfirmDialogueModalComponent, {
      title: label,
      action: {type: row.actif, value: title},

    }).subscribe((response) => {
      if (response) {
        //  this.getListUser();
      } else {
        // this.getListUser();
      }
    });
  }

  /**
   *
   * @param $row
   */
  setHabilitations($row) {
    this.router.navigate(['habilitations-user', $row.id, 'details']);
  }
}
