import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {WorkflowCrudUserAphpRoutingModule} from './workflow-crud-user-aphp-routing.module';
import {WorkflowCrudUserPageComponent} from './workflow-crud-user-page/workflow-crud-user-page.component';
import {WorkflowAddUserComponent} from './workflow-add-user/workflow-add-user.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {WorkflowSharedModule} from '../workflow-shared/workflow-shared.module';
import {HttpClient} from '@angular/common/http';
import {HttpLoaderFactory} from '../../app.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {ListUserAdminComponent} from './list-user-admin/list-user-admin.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ConfirmUserLdapComponent} from './confirm-user-ldap/confirm-user-ldap.component';
import { FormAddUserLdapComponent } from './form-add-user-ldap/form-add-user-ldap.component';
import {NgSlimScrollModule} from 'ngx-slimscroll';
import { GererHabilitationsUserComponent } from './gerer-habilitations-user/gerer-habilitations-user.component';

@NgModule({
  declarations: [
    WorkflowCrudUserPageComponent,
    WorkflowAddUserComponent,
    ListUserAdminComponent,
    ConfirmUserLdapComponent,
    FormAddUserLdapComponent,
    GererHabilitationsUserComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgSlimScrollModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    WorkflowCrudUserAphpRoutingModule,
    WorkflowSharedModule,
    NgbModule.forRoot(),
  ],
  entryComponents: [FormAddUserLdapComponent]
})
export class WorkflowCrudUserAphpModule {
}
