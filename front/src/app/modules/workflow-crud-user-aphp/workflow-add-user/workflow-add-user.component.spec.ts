import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkflowAddUserComponent } from './workflow-add-user.component';

describe('WorkflowAddUserComponent', () => {
  let component: WorkflowAddUserComponent;
  let fixture: ComponentFixture<WorkflowAddUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkflowAddUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowAddUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
