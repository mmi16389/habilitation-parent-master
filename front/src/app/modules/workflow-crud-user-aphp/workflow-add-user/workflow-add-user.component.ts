import { Component, OnInit } from '@angular/core';
import { CrudUserAphpService } from '../../../services/crud-user-aphp.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ShowErrorsInputFieldComponent } from '../../workflow-shared/show-errors-input-field/show-errors-input-field.component';
import { ActivatedRoute, Router } from '@angular/router';
import { ISlimScrollOptions } from 'ngx-slimscroll';
import { Location } from '@angular/common';
import { AbstractSharedComponent } from '../../workflow-transverse/abstract-shared/abstract-shared.component';
import { HabilitationsUserService } from 'src/app/services/habilitations-user.service';


@Component({
  selector: 'app-workflow-add-user',
  templateUrl: './workflow-add-user.component.html',
  styleUrls: ['./workflow-add-user.component.scss']
})
export class WorkflowAddUserComponent extends AbstractSharedComponent implements OnInit {
  /**
   *
   */
  public btnSubmit = 'affecter';
  /**
   *
   */
  public opts: ISlimScrollOptions;
  /**
   *
   */
  public states = [];

  /**
   *
   */
  public groups = [];
  /**
   *
   */
  public sites = [];

  /**
   *
   */
  public addUserLDAPForm: FormGroup = new FormGroup({});

  /**
   *
   */
  public blocSelect: boolean;
  /**
   *
   */
  public showBtn: boolean;

  /**
   *
   */
  public detailsSpinner = true;


  /**
   *
   */
  public userLDAPFilter: any;
  /**
   *
   */
  public user: any;
  /**
   *
   */
  public showWarning: boolean;
  /**
   *
   */
  private listUser = [];
  /**
   *
   */
  public userLDAP: any;


  /**
   *
   * @param apiService
   */
  constructor(private apiService: CrudUserAphpService,
    private habilitions: HabilitationsUserService,
    private router: Router,
    private route: ActivatedRoute, public _location: Location) {
    super(_location);
    this.route.params.subscribe((params: any) => {

    });
  }

  ngOnInit() {
    this.opts = {
      barBackground: '#00428b',
      gridBackground: 'transparent',
      barBorderRadius: '1',
      barWidth: '5',
      gridWidth: '3'
    };
    this.initializeForm();
    this.getGroupsAPHP();
    this.getSiteAPHP();
    this.getListUser();
  }

  /**
   *Initialiser le formulaire
   */
  private initializeForm() {
    this.addUserLDAPForm = new FormGroup({
      'userLDAP': new FormControl(null),
      'site': new FormControl(null)
    })
      ;
  }


  /**
   *
   */
  getGroupsAPHP() {
    this.habilitions.getGroupsAPHP().subscribe((dta: any) => {
      this.groups = dta;
    });
  }

  /**
   *
   */
  getSiteAPHP() {
    this.habilitions.getSiteAPHP().subscribe((dta: any) => {
      this.sites = dta;
    });
  }

  /**
   *
   * @param {FormGroup} element
   */
  onSubmit(element: FormGroup) {
    if (element.valid) {
      if (!this.checkExitingUser(this.user.codeAph)) {
        this.addNewUserLDAP(this.user);
      } else {
        // console.log(' dkldkldkldskds ');
        this.router.navigate(['habilitations-user/'+this.user.id+'/details']);
      }
    } else if (!element.valid) {
      let objForm = new ShowErrorsInputFieldComponent();
      objForm.notValideForm(element);
      objForm = null;
    }
  }

  /**
   *
   * @param user
   */
  addNewUserLDAP(user: any) {
    this.detailsSpinner = false;
    this.apiService.enregistrerUSerLADP(user).subscribe((data) => {
      this.detailsSpinner = true;
      this.router.navigate(['confirm-user-ldap', { userLdap: JSON.stringify(this.user) }]);
    },
      error => {
        this.detailsSpinner = true;
      });
  }


  /**
   *
   * @param elem
   */
  filterUserLDAP(elem: any) {
    this.blocSelect = true;
    this.showWarning = false;
    this.addUserLDAPForm.get('userLDAP').valueChanges.subscribe((dta) => {
      if (dta.length >= 2 && dta !== '') {
        this.apiService.getUserLDAP().subscribe((data: any) => {
          this.states = data;
          this.userLDAPFilter = this.states.filter((el: any) => {
            if (el['nomUsuel']) {
              return el['nomUsuel'];
            }
            return false;
          });
        });
      } else {
        this.blocSelect = false;
        this.userLDAPFilter = [];
      }

    });
  }

  /**
   *
   * @param user
   */
  selectUserLDAP(user: any) {

    this.user = user;
    this.userLDAPFilter = [];
    this.blocSelect = null;
    this.showBtn = true;

    this.addUserLDAPForm.get('userLDAP').setValue(user.nomUsuel + ' ' + user.prenom);
    // TODO  to change service ready
    this.addUserLDAPForm.get('site').setValue(this.sites[0].value);
    // TODO End
    if (this.checkExitingUser(this.user.codeAph)) {
      this.showWarning = true;
      this.btnSubmit = 'Gérer ses droits';
    }
  }

  /**
   *
   */
  getListUser() {
    this.apiService.getListUser().subscribe((data: any) => {
      this.listUser = data;
    });
  }

  /**
   *
   * @param codeaph
   */
  checkExitingUser(codeaph: string) {
    let check = false;
    this.listUser.filter((el: any) => {
      if (el.codeAph === codeaph) {
        check = true;
        return check;
      } else {
        return check;
      }
    });
    return check;
  }
}
