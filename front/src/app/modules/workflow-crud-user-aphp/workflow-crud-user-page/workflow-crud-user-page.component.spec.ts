import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkflowCrudUserPageComponent } from './workflow-crud-user-page.component';

describe('WorkflowCrudUserPageComponent', () => {
  let component: WorkflowCrudUserPageComponent;
  let fixture: ComponentFixture<WorkflowCrudUserPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkflowCrudUserPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowCrudUserPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
