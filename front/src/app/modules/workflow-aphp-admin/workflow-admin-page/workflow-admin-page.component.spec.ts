import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkflowAdminPageComponent } from './workflow-admin-page.component';

describe('WorkflowAdminPageComponent', () => {
  let component: WorkflowAdminPageComponent;
  let fixture: ComponentFixture<WorkflowAdminPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkflowAdminPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowAdminPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
