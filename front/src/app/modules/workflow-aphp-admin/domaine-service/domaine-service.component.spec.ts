import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomaineServiceComponent } from './domaine-service.component';

describe('DomaineServiceComponent', () => {
  let component: DomaineServiceComponent;
  let fixture: ComponentFixture<DomaineServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomaineServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomaineServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
