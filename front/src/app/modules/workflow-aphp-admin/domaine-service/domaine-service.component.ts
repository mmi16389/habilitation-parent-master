import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import {ShowErrorsInputFieldComponent} from '../../workflow-shared/show-errors-input-field/show-errors-input-field.component';

@Component({
  selector: 'app-domaine-service',
  templateUrl: './domaine-service.component.html',
  styleUrls: ['./domaine-service.component.scss']
})
export class DomaineServiceComponent implements OnInit {

  /**
   *
   */
  public model = [];
  /**
   *
   */
  public groups = [];
  /**
   *
   */
  public sites = [];
  /**
   *
   */
  public gpCheccked = false;

  /**
   *
   */
  public services = [];

  /**
   *
   */
  public addDomaineServiceForm: FormGroup = new FormGroup({});

  /**
   *
   * @param apiService
   */
  constructor() {
  }

  ngOnInit() {
    this.initializeForm();
  }

  /**
   *Initialiser le formulaire
   */
  private initializeForm() {
    this.addDomaineServiceForm = new FormGroup({
      'service': new FormControl(null),
      'site': new FormControl(null),
      'groupes': new FormControl(null)
    })
    ;
  }

  /**
   *
   * @param {FormGroup} element
   */
  onSubmit(element: FormGroup) {
    if (element.valid) {
      console.log(element);
    }
    else if (!element.valid) {
      console.log(' ERROR VALUE ', element);
      let objForm = new ShowErrorsInputFieldComponent();
      objForm.notValideForm(element);
      objForm = null;
    }
  }

}
