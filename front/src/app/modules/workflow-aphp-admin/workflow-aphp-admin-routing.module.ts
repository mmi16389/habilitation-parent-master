import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WorkflowAdminPageComponent} from './workflow-admin-page/workflow-admin-page.component';
import {DomaineCategorieComponent} from './domaine-categorie/domaine-categorie.component';
import { DomaineServiceComponent } from './domaine-service/domaine-service.component';

const routes: Routes = [
  {
    path: 'admin',
    component: WorkflowAdminPageComponent
  },
  {
    path: 'admin',
    children: [
      {
        path: 'domaine-categorie',
        component: DomaineCategorieComponent
      }
      ,
      {
        path: 'domaine-service',
        component: DomaineServiceComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkflowAphpAdminRoutingModule {
}
