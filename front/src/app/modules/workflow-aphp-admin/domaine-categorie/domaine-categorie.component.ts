import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-domaine-categorie',
  templateUrl: './domaine-categorie.component.html',
  styleUrls: ['./domaine-categorie.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DomaineCategorieComponent implements OnInit {

  rows = [];

  constructor() {
    this.fetch((data) => {
      this.rows = data;
    });
  }

  ngOnInit() {
  }

  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/100k.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  /**
   *
   * @param row
   */
  public getRowClass(row) {
    console.log(row.gender);
    return {
      'ngx-cell': true,
      'ngx-cell-each-column': row.gender === 'male'
    };
  }

  /**
   *
   * @param row
   * @param column
   * @param value
   */
  public getCellClass({row, column, value}): any {
    return {
      'text-ngx-table': true,
    };
  }
}
