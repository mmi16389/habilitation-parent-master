import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomaineCategorieComponent } from './domaine-categorie.component';

describe('DomaineCategorieComponent', () => {
  let component: DomaineCategorieComponent;
  let fixture: ComponentFixture<DomaineCategorieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomaineCategorieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomaineCategorieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
