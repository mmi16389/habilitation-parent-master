import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WorkflowAphpAdminRoutingModule} from './workflow-aphp-admin-routing.module';
import {WorkflowAdminPageComponent} from './workflow-admin-page/workflow-admin-page.component';
import {ProfilUserComponent} from './profil-user/profil-user.component';
import {DomaineCategorieComponent} from './domaine-categorie/domaine-categorie.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../app.module';
import {HttpClient} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DomaineServiceComponent } from './domaine-service/domaine-service.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WorkflowSharedModule } from '../workflow-shared/workflow-shared.module';


@NgModule({
  declarations: [
    WorkflowAdminPageComponent,
    ProfilUserComponent,
    DomaineCategorieComponent,
    DomaineServiceComponent
  ],
  imports: [
    CommonModule,
    WorkflowAphpAdminRoutingModule,
    NgxDatatableModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    WorkflowSharedModule,
    NgbModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ]
})
export class WorkflowAphpAdminModule {
}
