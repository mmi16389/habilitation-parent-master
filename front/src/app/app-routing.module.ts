import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WorkflowCrudUserPageComponent} from './modules/workflow-crud-user-aphp/workflow-crud-user-page/workflow-crud-user-page.component';

const routes: Routes = [
    {
      path: '',
      redirectTo: '/home-page',
      pathMatch: 'full'
    },
    {
      path: 'acceuil',
      component: WorkflowCrudUserPageComponent
    }
  ]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
