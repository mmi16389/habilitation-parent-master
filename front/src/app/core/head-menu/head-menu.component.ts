import {Component, OnInit} from '@angular/core';
import {User} from '../../models/user'; 

@Component({
  selector: 'app-head-menu',
  templateUrl: './head-menu.component.html',
  styleUrls: ['./head-menu.component.scss']
})
export class HeadMenuComponent implements OnInit {
  /**
   *
   */
  public model: any;
  /**
   *
   */
  public showSubSetting = true;
  /**
   *
   */
  public user: User;

  constructor() {
  }

  ngOnInit() {
    if (localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
  }
 
  /**
   *
   * @param $element
   */
  setLanguage($element) {
    localStorage.setItem('lang', $element);
  }

  /**
   *
   * @param $event
   */
  showParam($event) {
    this.showSubSetting = false;
  }


}
